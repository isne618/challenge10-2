#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

void draw_grid(int grid[][7])
{
	cout << endl;
	cout << "-----------------------------------------" << endl;
	for (int i = 5; i >= 0; i--)
	{
		for (int j = 0; j < 7; j++)
		{
			if (grid[i][j] == 1)
			{
				cout << "| X | "; //for player
			}
			else if (grid[i][j] == 2)
			{
				cout << "| O | "; //for AI
			}
			else
			{
				cout << "| - | ";
			}
		}
		cout << endl << "-----------------------------------------" << endl;
	}
	cout << "  1     2     3     4     5     6     7" << endl;
}

void player_turn(int grid[][7]) //player select number to drop
{
	int column, row = 0, turn = 1;

	cout << "Please enter number of the column: ";
	cin >> column;

	while (column < 1 || column > 7) //if player input numbernot in [1-7]
	{
		cout << "**You can only input [1-7], Please enter again**" << endl;
		cout << "Please enter number of the column: ";
		cin >> column;
	}

	while (grid[5][column - 1] != 0) //check column available
	{
		cout << "**This column is not available**";
		cout << "Please enter number of the column: ";
		cin >> column;
	}

	while (grid[row][column - 1] != 0) //if not available then row++
	{
		row++;
	}

	grid[row][column - 1] = turn; //available = drop
	
}

void ai_turn(int grid[][7])
{
	int turn = 2, row = 0,check = 0, random; //check --> to check is that place that random is available or not

	srand(time(0));
	do //if it not empty it will random again
	{
		check = 0;
		random = rand() % 7 + 1;

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (grid[i + 1][j + 1] == 2 && grid[i + 2][j + 2] == 2 && grid[i + 3][j + 3] == 0 && grid[i][j] == 2)
				{
					if (i == 0 && j == 0)
					{
						random = 4;
					}
					else if (i == 1 && j == 1)
					{
						random = 5;
					}
					else if (i == 2 && j == 2)
					{
						random = 6;
					}
				}
			}
		}
		for (int i = 5; i >= 3; i--)
		{
			for (int j = 0; j < 4; j++)
			{
				if (grid[i - 1][j + 1] == 2 && grid[i - 2][j + 2] == 2 && grid[i - 3][j + 3] == 0 && grid[i][j] == 2)
				{
					if (i == 5 && j == 0)
					{
						random = 4;
					}
					else if (i == 4 && j == 1)
					{
						random = 5;
					}
					else if (i == 3 && j == 2)
					{
						random = 6;
					}
				}
			}
		}

		for (int j = 0; j < 7; j++)
		{
			for (int i = 0; i < 3; i++)
			{
				if (grid[i + 1][j] == 2 && grid[i + 2][j] == 2 && grid[i + 3][j] == 0 && grid[i][j] == 2)
				{
					if (j == 0)
					{
						random = 1;
					}
					else if (j == 1)
					{
						random = 2;
					}
					else if (j == 2)
					{
						random = 3;
					}
					else if (j == 3)
					{
						random = 4;
					}
					else if (j == 4)
					{
						random = 5;
					}
					else if (j == 5)
					{
						random = 6;
					}
					else if (j == 6)
					{
						random = 7;
					}
				}
			}
		}

		//Check AI win horizontal strategy
		for (int i = 0; i < 6; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (grid[i][j + 1] == 2 && grid[i][j + 2] == 2 && grid[i][j + 3] == 0 && grid[i][j] == 2)
				{
					if (j == 0)
					{
						random = 4;
					}
					else if (j == 1)
					{
						random = 5;
					}
					else if (j == 2)
					{
						random = 6;
					}
					else if (j == 3)
					{
						random = 7;
					}
				}
				else if (grid[i][j + 1] == 2 && grid[i][j + 2] == 2 && grid[i][j + 3] == 2 && grid[i][j] == 0)
				{
					if (j == 6)
					{
						random = 4;
					}
					else if (j == 5)
					{
						random = 3;
					}
					else if (j == 4)
					{
						random = 2;
					}
					else if (j == 3)
					{
						random = 1;
					}
				}
				else if (grid[i][j + 1] == 0 && grid[i][j + 2] == 2 && grid[i][j + 3] == 2 && grid[i][j] == 2)
				{
					if (j == 0)
					{
						random = 2;
					}
					else if (j == 1)
					{
						random = 3;
					}
					else if (j == 2)
					{
						random = 4;
					}
					else if (j == 3)
					{
						random = 5;
					}
				}
				else if (grid[i][j + 1] == 2 && grid[i][j + 2] == 0 && grid[i][j + 3] == 2 && grid[i][j] == 2)
				{
					if (j == 0)
					{
						random = 3;
					}
					else if (j == 1)
					{
						random = 4;
					}
					else if (j == 2)
					{
						random = 5;
					}
					else if (j == 3)
					{
						random = 6;
					}
				}
			}
		}

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (grid[i + 1][j + 1] == 1 && grid[i + 2][j + 2] == 1 && grid[i + 3][j + 3] == 0 && grid[i][j] == 1)
				{
					if (i == 0 && j == 0)
					{
						random = 4;
					}
					else if (i == 1 && j == 1)
					{
						random = 5;
					}
					else if (i == 2 && j == 2)
					{
						random = 6;
					}
				}
			}
		}
		for (int i = 5; i >= 3; i--)
		{
			for (int j = 0; j < 4; j++)
			{
				if (grid[i - 1][j + 1] == 1 && grid[i - 2][j + 2] == 1 && grid[i - 3][j + 3] == 0 && grid[i][j] == 1)
				{
					if (i == 5 && j == 0)
					{
						random = 4;
					}
					else if (i == 4 && j == 1)
					{
						random = 5;
					}
					else if (i == 3 && j == 2)
					{
						random = 6;
					}
				}
			}
		}

		for (int j = 0; j < 7; j++)
		{
			for (int i = 0; i < 3; i++)
			{
				if (grid[i + 1][j] == 1 && grid[i + 2][j] == 1 && grid[i + 3][j] == 0 && grid[i][j] == 1)
				{
					if (j == 0)
					{
						random = 1;
					}
					else if (j == 1)
					{
						random = 2;
					}
					else if (j == 2)
					{
						random = 3;
					}
					else if (j == 3)
					{
						random = 4;
					}
					else if (j == 4)
					{
						random = 5;
					}
					else if (j == 5)
					{
						random = 6;
					}
					else if (j == 6)
					{
						random = 7;
					}
				}
			}
		}

		for (int i = 0; i < 6; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (grid[i][j + 1] == 1 && grid[i][j + 2] == 1 && grid[i][j + 3] == 0 && grid[i][j] == 1)
				{
					if (j == 0)
					{
						random = 4;
					}
					else if (j == 1)
					{
						random = 5;
					}
					else if (j == 2)
					{
						random = 6;
					}
					else if (j == 3)
					{
						random = 7;
					}
				}
				else if (grid[i][j + 1] == 1 && grid[i][j + 2] == 1 && grid[i][j + 3] == 1 && grid[i][j] == 0)
				{
					if (j == 6)
					{
						random = 4;
					}
					else if (j == 5)
					{
						random = 3;
					}
					else if (j == 4)
					{
						random = 2;
					}
					else if (j == 3)
					{
						random = 1;
					}
				}
				else if (grid[i][j + 1] == 0 && grid[i][j + 2] == 1 && grid[i][j + 3] == 1 && grid[i][j] == 1)
				{
					if (j == 0)
					{
						random = 2;
					}
					else if (j == 1)
					{
						random = 3;
					}
					else if (j == 2)
					{
						random = 4;
					}
					else if (j == 3)
					{
						random = 5;
					}
				}
				else if (grid[i][j + 1] == 1 && grid[i][j + 2] == 0 && grid[i][j + 3] == 1 && grid[i][j] == 1)
				{
					if (j == 0)
					{
						random = 3;
					}
					else if (j == 1)
					{
						random = 4;
					}
					else if (j == 2)
					{
						random = 5;
					}
					else if (j == 3)
					{
						random = 6;
					}
				}
			}
		}

		if (grid[5][random - 1] != 0)
		{
			check = 1;
		}

	} while (check == 1); 

	while (grid[row][random - 1] != 0) //if it not empty
	{
		row++;
	}

	grid[row][random - 1] = turn; //if random place is available ai will drop
}

int check_win(int grid[][7])
{
	int win = 0;
	for (int i = 0; i < 3; i++) //slope up win
	{
		for (int j = 0; j < 4; j++)
		{
			if (grid[i + 1][j + 1] == 1 && grid[i + 2][j + 2] == 1 && grid[i + 3][j + 3] == 1 && grid[i][j] == 1)
			{
				return win = 1;
			}
			else if (grid[i + 1][j + 1] == 2 && grid[i + 2][j + 2] == 2 && grid[i + 3][j + 3] == 2 && grid[i][j] == 2)
			{
				return win = 2;
			}
		}
	}

	for (int i = 5; i >= 3; i--) //slope down win
	{
		for (int j = 0; j < 4; j++)
		{
			if (grid[i - 1][j + 1] == 1 && grid[i - 2][j + 2] == 1 && grid[i - 3][j + 3] == 1 && grid[i][j] == 1)
			{
				return win = 1;
			}
			else if (grid[i - 1][j + 1] == 2 && grid[i - 2][j + 2] == 2 && grid[i - 3][j + 3] == 2 && grid[i][j] == 2)
			{
				return win = 2;
			}
		}
	}

	for (int j = 0; j < 7; j++) //vertical win
	{
		for (int i = 0; i < 3; i++)
		{
			if (grid[i + 1][j] == 1 && grid[i + 2][j] == 1 && grid[i + 3][j] == 1 && grid[i][j] == 1)
			{
				return win = 1;
			}
			else if (grid[i + 1][j] == 2 && grid[i + 2][j] == 2 && grid[i + 3][j] == 2 && grid[i][j] == 2)
			{
				return win = 2;
			}
		}
	}

	for (int i = 0; i < 6; i++) //horizontal win
	{
		for (int j = 0; j < 4; j++)
		{
			if (grid[i][j + 1] == 1 && grid[i][j + 2] == 1 && grid[i][j + 3] == 1 && grid[i][j] == 1)
			{
				return win = 1;
			}
			else if (grid[i][j + 1] == 2 && grid[i][j + 2] == 2 && grid[i][j + 3] == 2 && grid[i][j] == 2)
			{
				return win = 2;
			}
		}
	}
}

void main()
{
	char answer;
	int turn = 0, round = 0,
		grid[6][7] = { 0 };

	cout << "        **---CONNECT FOUR---**           " << endl;
	cout << "-----------------------------------------" << endl;
	cout << "| - | | - | | - | | - | | - | | - | | - |" << endl;
	cout << "-----------------------------------------" << endl;
	cout << "| - | | - | | - | | - | | - | | - | | - |" << endl;
	cout << "-----------------------------------------" << endl;
	cout << "| - | | - | | - | | - | | - | | - | | - |" << endl;
	cout << "-----------------------------------------" << endl;
	cout << "| - | | - | | - | | - | | - | | - | | - |" << endl;
	cout << "-----------------------------------------" << endl;
	cout << "| - | | - | | - | | - | | - | | - | | - |" << endl;
	cout << "-----------------------------------------" << endl;
	cout << "| - | | - | | - | | - | | - | | - | | - |" << endl;
	cout << "-----------------------------------------" << endl;
	cout << "  1     2     3     4     5     6     7" << endl;
	cout << endl << "     Player --> 'X' VS AI --> 'O'" << endl;
	cout << endl << "Do you want to go first (y/n): "; //y --> yes / n--> no
	cin >> answer;

	while (answer != 'y' && answer != 'n') //if input is not 'y' or 'n'
	{
		cout << "Please input only 'y' or 'n', input again: ";
		cin >> answer;
	}
	if (answer == 'y')
	{
		turn = 1; //1 for player
	}
	else if (answer == 'n')
	{
		turn = 2; //2 for AI
	}

	while (round <= 42)
	{
		if (turn == 1)  //player
		{
			cout << endl << "**Player's Turn**" << endl;
			player_turn(grid);
			draw_grid(grid);
			turn = 2;
		}
		else if (turn == 2)  //ai
		{
			cout << endl << "**Ai's Turn**" << endl;
			ai_turn(grid);
			draw_grid(grid);
			turn = 1;
		}
		round++; 
		if (check_win(grid) == 1)
		{
			cout << endl << "--Congratulation. You won!!--" << endl;
			break;
		}
		else if (check_win(grid) == 2)
		{
			cout << endl << "--You Lose. The winner is AI!!--" << endl;
			break;
		}
		else if (round == 42)
		{
			cout << endl << "--It's tie.--" << endl;
		}
	}
}